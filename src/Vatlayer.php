<?php

namespace Germangutierrezv\Vatlayer;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class Vatlayer
{
    /**
     * Client HTTP instance
     */
    private $httpClient;

    /**
     * Vatlayer constructor
     *
     * @param  \GuzzleHttp\Client  $httpClient
     */
    public function __construct()
    {
        $this->httpClient = new Client;
    }

    /**
     * Validate VAT number retrieve data information
     *
     * @param string $vatNumber
     * @param int $format 1 = JSON
     * @return array|null
     */
    public function validate($vatNumber, $format = 1)
    {
        $url = $this->buildUrl('validate', [
            'vat_number' => $vatNumber,
            'format' => $format
        ]);
        
        return $this->execute($url);
    }

    /**
     * Determines if VAT number is valid.
     *
     * @param string $vatNumber
     * @return \Illuminate\Http\JsonResponse
     */
    public function isValidVatNumber($vatNumber)
    {
        try {
            $response = $this->validate($vatNumber);
        } catch (Exception $exception) {
            return false;
        }

        return boolval(data_get($response, 'valid', false));
    }

    /**
     * Execute request and parse the results.
     */
    protected function execute($url)
    {
        try {
            $response = $this->httpClient->get($url);
            
            if ($response->getStatusCode() === Response::HTTP_OK) {
                $attributes = json_decode($response->getBody()->getContents(), TRUE);
    
                return $attributes;
            }
        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $exception->getCode(), 
                    'info' => $exception->getMessage() !== ''? $exception->getMessage() : 'Not found'
                ]],
                $exception->getCode() == 0 ? 404 : $exception->getCode());
        }
    }

    /**
     * Build URL which needs to be called.
     *
     * @param  string  $action
     * @param  array  $parameters
     * @return string
     */
    protected function buildUrl($action, $parameters)
    {
        $parameters['access_key'] = config('vatlayer.api_key');

        $api_url = config('vatlayer.api_url');

        $encrypted = config('vatlayer.api_url_encrypted');

        return sprintf(
            '%s://%s/%s?%s',
            $encrypted,
            $api_url,
            $action,
            http_build_query($parameters)
        );
    }
}