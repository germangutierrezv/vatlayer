<?php

namespace Germangutierrezv\Vatlayer\Exceptions;

use Exception;
use Throwable;

class RequestFailed extends Exception
{
    /**
     * Constructor exception
     * 
     * @param int $code
     * @param string $message
     * @param Throwable $previous
     */
    public function __construct($code = 0, $message = "", Throwable $previous = NULL)
    {
        parent::__construct(
            'Request failed, code: ' . $code . '. message: ' . $message,
            $code,
            $previous
        );
    }
}