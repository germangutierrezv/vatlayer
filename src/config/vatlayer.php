<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Key Vatlayer
    |--------------------------------------------------------------------------
    |
    | This value is the API Key Vatlayer.
    |
    */

    'api_key' => env('VATLAYER_API_KEY', ''),
    /*
    |--------------------------------------------------------------------------
    | API Url Vatlayer
    |--------------------------------------------------------------------------
    |
    | This value is the API Url Vatlayer.
    |
    */

    'api_url_encrypted' => env('VATLAYER_API_ENCRYPTED', 'http'),

    /*
    |--------------------------------------------------------------------------
    | API Url Vatlayer
    |--------------------------------------------------------------------------
    |
    | This value is the API Url Vatlayer.
    |
    */

    'api_url' => env('VATLAYER_API_URL', 'apilayer.net/api'),
    

];