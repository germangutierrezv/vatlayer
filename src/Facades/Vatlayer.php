<?php

namespace Germangutierrezv\Vatlayer\Facades;

use Illuminate\Support\Facades\Facade;

class Vatlayer extends Facade 
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     */
     protected static function getFacadeAccessor()
     {
        return 'vatlayer';
     }
}