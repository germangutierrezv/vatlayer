<?php

namespace Germangutierrezv\Vatlayer;

use Germangutierrezv\Vatlayer\Facades\Vatlayer;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->publishes([
            __DIR__ . '/config/vatlayer.php' => config_path('vatlayer.php'),
        ]);

        $this->mergeConfigFrom(
            __DIR__ . '/config/vatlayer.php', 'vatlayer'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('vatlayer', function () {
            return new Vatlayer();
        });
    }
}
