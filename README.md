## Laravel Vatlayer

Laravel Vatlayer es un paquete que permite conectar una aplicación laravel con el [Vatlayer API](https://vatlayer.com/).

## Instalación
La instalación es rápida y sencilla. Instala el paquete via Composer y ya está listo.
```
composer require germangutierrezv/vatlayer
```

## Configuración
El paquete necesita de un API KEY obtenida desde el Vatlayer API. Puedes solicitar una desde [aquí](https://vatlayer.com/). Una vez obtenido el API KEY agregar en el archivo de configuración de entorno `.env` de la aplicación la siguiente variable:
```
VATLAYER_API_KEY=API KEY
```
## Uso
Actualmente el paquete sólo incluye dos métodos utilizables. 

```
use Germangutierrezv\Vatlayer\Vatlayer;

// ...
```

Inicializa 
```
$vatlayer = new Vatlayer(); 
```

### Ejecutar una solicitud de validación

```
$valayer->validate('LU26375245');
```

Si es valido VAT retorna en formato JSON la información 
```
{
  "valid": true,
  "database": "ok",
  "format_valid": true,
  "query": "LU26375245",
  "country_code": "LU",
  "vat_number": "26375245",
  "company_name": "AMAZON EUROPE CORE S.A R.L.",
  "company_address": "5, RUE PLAETIS L-2338 LUXEMBOURG"
}   
```

### Comprueba si VAT es válido
```
$vatlayer->isValidVatNumber('LU26375245');
```
Retorna en formato JSON el campo validate como un booleano si el número tiene un formato válido y es un número de VAT europeo conocido.
